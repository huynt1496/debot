module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ['plugin:react/recommended', 'airbnb'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    'import/order': [
      'error',
      { alphabetize: { order: 'asc' }, 'newlines-between': 'always' },
    ],
    'import/prefer-default-export': 'warn',
    'jsx-a11y/label-has-associated-control': 'off',
    'max-len': 'off',
    'no-console': 'off',
    'no-eval': 'off',
    'no-param-reassign': 'warn',
    'no-underscore-dangle': 'off',
    'no-unreachable': 'warn',
    'no-unused-vars': 'warn',
    'prefer-destructuring': ['error', { object: true, array: false }],
    'react/forbid-prop-types': 'warn',
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'react/jsx-props-no-spreading': 'off',
    'react/no-array-index-key': 'off',
    'react/no-unescaped-entities': 'warn',
    'react/prop-types': 'warn',
    'react/react-in-jsx-scope': 'off',
  },
};
