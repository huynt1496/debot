import axios from 'axios';
import moment from 'moment';

import {
  BACKEND_URL,
} from './config';

export const sendRequest = (
  action,
  params = {},
  responseType = 'json',
) => new Promise((resolve, reject) => {
  const fd = new FormData();
  fd.append('type', action);
  fd.append('data', JSON.stringify({
    ...params,
  }));
  const options = {
    responseType,
  };
  axios.post(`${BACKEND_URL}/userRequest`, fd, options).then((res) => {
    resolve(res.data);
  }).catch((res) => {
    reject(res);
  });
});

export const parseDate = (date) => moment(date).format('DD/MM/YYYY');

export const departmentUrls = {
  '01': {
    url: 'https://hanoi.edu.vn/',
    crawled: false,
  },
  '02': {
    url: 'http://hagiang.edu.vn/',
    crawled: false,
  },
  '03': {
    url: 'https://hcm.edu.vn/homehcm',
    crawled: true,
  },
  '04': {
    url: 'https://sogiaoduc.caobang.gov.vn/',
    crawled: false,
  },
  '06': {
    url: 'https://backan.edu.vn/',
    crawled: false,
  },
  '08': {
    url: 'http://tuyenquang.edu.vn/',
    crawled: false,
  },
  10: {
    url: 'http://laocai.edu.vn/',
    crawled: false,
  },
  11: {
    url: 'https://dienbien.edu.vn/',
    crawled: false,
  },
  12: {
    url: 'https://laichau.edu.vn/',
    crawled: false,
  },
  14: {
    url: 'http://sogddtsonla.edu.vn/',
    crawled: false,
  },
  15: {
    url: 'https://yenbai.edu.vn/',
    crawled: false,
  },
  16: {
    url: 'https://haiphong.edu.vn/',
    crawled: false,
  },
  17: {
    url: 'https://hoabinh.edu.vn/',
    crawled: false,
  },
  19: {
    url: 'http://thainguyen.edu.vn/',
    crawled: false,
  },
  20: {
    url: 'https://langson.edu.vn/',
    crawled: false,
  },
  22: {
    url: 'https://www.quangninh.gov.vn/',
    crawled: false,
  },
  24: {
    url: 'https://sgd.bacgiang.gov.vn/',
    crawled: false,
  },
  25: {
    url: 'https://phutho.edu.vn/',
    crawled: false,
  },
  26: {
    url: 'https://vinhphuc.edu.vn/',
    crawled: false,
  },
  27: {
    url: 'https://bacninh.edu.vn/',
    crawled: false,
  },
  30: {
    url: 'http://haiduong.edu.vn/',
    crawled: false,
  },
  33: {
    url: 'https://hungyen.edu.vn/',
    crawled: false,
  },
  34: {
    url: 'https://thaibinh.edu.vn/',
    crawled: false,
  },
  35: {
    url: 'https://sgddt.hanam.gov.vn/',
    crawled: false,
  },
  36: {
    url: 'https://namdinh.edu.vn/',
    crawled: false,
  },
  37: {
    url: 'https://ninhbinh.edu.vn/',
    crawled: false,
  },
  38: {
    url: 'https://sgddt.khanhhoa.gov.vn/',
    crawled: false,
  },
  40: {
    url: 'https://nghean.edu.vn/',
    crawled: false,
  },
  42: {
    url: 'https://hatinh.edu.vn/',
    crawled: false,
  },
  44: {
    url: 'https://sgddt.quangbinh.gov.vn/',
    crawled: false,
  },
  45: {
    url: 'https://quangtri.edu.vn/',
    crawled: false,
  },
  46: {
    url: 'https://thuathienhue.edu.vn/',
    crawled: false,
  },
  48: {
    url: 'https://www.danang.edu.vn/',
    crawled: false,
  },
  49: {
    url: 'https://quangnam.edu.vn/',
    crawled: false,
  },
  51: {
    url: 'https://quangngai.edu.vn/',
    crawled: false,
  },
  52: {
    url: 'https://sgddt.binhdinh.gov.vn/',
    crawled: false,
  },
  54: {
    url: 'https://phuyen.edu.vn/',
    crawled: false,
  },
  56: {
    url: 'https://khanhhoa.edu.vn/',
    crawled: true,
  },
  58: {
    url: 'http://ninhthuan.edu.vn/',
    crawled: true,
  },
  60: {
    url: 'https://sgddt.binhthuan.gov.vn/',
    crawled: true,
  },
  62: {
    url: 'http://kontum.edu.vn/',
    crawled: true,
  },
  64: {
    url: 'http://gialai.edu.vn/',
    crawled: true,
  },
  66: {
    url: 'http://gddt.daklak.gov.vn/',
    crawled: true,
  },
  67: {
    url: 'http://Daknong.edu.vn/',
    crawled: true,
  },
  68: {
    url: 'https://lamdong.edu.vn/',
    crawled: true,
  },
  70: {
    url: 'http://binhphuoc.edu.vn/',
    crawled: true,
  },
  72: {
    url: 'http://tayninh.edu.vn/',
    crawled: true,
  },
  74: {
    url: 'https://binhduong.edu.vn/',
    crawled: true,
  },
  75: {
    url: 'http://sgddt.dongnai.gov.vn/',
    crawled: true,
  },
  77: {
    url: 'http://bariavungtau.edu.vn/',
    crawled: true,
  },
  80: {
    url: 'https://qlgd.longan.edu.vn/',
    crawled: false,
  },
  82: {
    url: 'https://sgddt.tiengiang.gov.vn/',
    crawled: true,
  },
  83: {
    url: 'https://www.bentre.edu.vn/',
    crawled: false,
  },
  84: {
    url: 'http://sgdtravinh.edu.vn/',
    crawled: true,
  },
  86: {
    url: 'https://vinhlong.edu.vn/',
    crawled: true,
  },
  87: {
    url: 'http://dongthap.edu.vn/',
    crawled: true,
  },
  89: {
    url: 'https://angiang.edu.vn/',
    crawled: true,
  },
  91: {
    url: 'https://sgddt.kiengiang.gov.vn/',
    crawled: true,
  },
  92: {
    url: 'http://cantho.edu.vn/',
    crawled: true,
  },
  93: {
    url: 'https://haugiang.edu.vn/',
    crawled: false,
  },
  94: {
    url: 'https://sogddt.soctrang.gov.vn/',
    crawled: false,
  },
  95: {
    url: 'https://sgddt.baclieu.gov.vn/',
    crawled: false,
  },
  96: {
    url: 'https://sogddt.camau.gov.vn/',
    crawled: false,
  },
};
