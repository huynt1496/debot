export const SIGN_OUT = 'SIGN_OUT';

export const CATALOGS = {
  handlers: {
    getCities: 'CATALOGS_GET_CITIES_HANDLER',
  },
  update: 'CATALOGS_UPDATE',
};

export const INFORMATION = {
  handlers: {
    fetchInfo: 'INFORMATION_FETCH_INFO_HANDLER',
  },
  update: 'INFORMATION_UPDATE',
};

export const WSPAGE = {
  handlers: {
    wsfetchPage: 'WORKING_SCHEDULE_PAGE_HANDLER',
  },
  update: 'WORKING_SCHEDULE_UPDATE',
};

export const NEWSPAGE = {
  handlers: {
    newsPageFetch: 'NEWS_PAGE_HANDLER',
  },
  update: 'NEWS_UPDATE',
};

export const NOTIFICATION_PAGE = {
  handlers: {
    notificationfetchPage: 'NOTIFICATION_PAGE_HANDLER',
  },
  update: 'NOTIFICATION_UPDATE',
};
