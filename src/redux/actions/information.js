import axios from 'axios';
import {
  all,
  call,
  takeEvery,
  put,
} from 'redux-saga/effects';

import {
  BACKEND_URL,
} from '../../libs/config';

import {
  INFORMATION, WSPAGE, NEWSPAGE, NOTIFICATION_PAGE,
} from './constants';

function* storeSystem(id, data) {
  yield put({
    type: INFORMATION.update,
    id,
    data,
  });
}

function* storeWsfetchPage(id, data) {
  yield put({
    type: WSPAGE.update,
    id,
    data,
  });
}

function* storeNewsPageFetch(id, data) {
  yield put({
    type: NEWSPAGE.update,
    id,
    data,
  });
}

function* storeNotificationfetchPage(id, data) {
  yield put({
    type: NOTIFICATION_PAGE.update,
    id,
    data,
  });
}

function* fetchInfo({ id }) {
  try {
    const infomation = yield call(() => new Promise((resolve, reject) => {
      const options = {
        responseType: 'json',
      };
      axios.post(`${BACKEND_URL}/crawl/department`, { province: id }, options).then((res) => {
        resolve(res.data);
      }).catch((res) => {
        reject(res);
      });
    }));
    yield* storeSystem(id, infomation);
  } catch (error) {
    yield* storeSystem(id);
    console.log(error);
  }
}

function* wsfetchPage({ id, pageCrawl }) {
  try {
    const infomation = yield call(() => new Promise((resolve, reject) => {
      const options = {
        responseType: 'json',
      };
      axios.post(
        `${BACKEND_URL}/crawl/department`,
        { province: id, contentPage: 'ws', page: pageCrawl },
        options,
      ).then((res) => {
        resolve(res.data);
      }).catch((res) => {
        reject(res);
      });
    }));
    yield* storeWsfetchPage(id, infomation);
  } catch (error) {
    console.log('error');
  }
}

function* newsPageFetch({ id, pageCrawl }) {
  try {
    const infomation = yield call(() => new Promise((resolve, reject) => {
      const options = {
        responseType: 'json',
      };
      axios.post(
        `${BACKEND_URL}/crawl/department`,
        { province: id, contentPage: 'news', page: pageCrawl },
        options,
      ).then((res) => {
        resolve(res.data);
      }).catch((res) => {
        reject(res);
      });
    }));
    yield* storeNewsPageFetch(id, infomation);
  } catch (error) {
    console.log('error');
  }
}

function* notificationfetchPage({ id, pageCrawl }) {
  try {
    const infomation = yield call(() => new Promise((resolve, reject) => {
      const options = {
        responseType: 'json',
      };
      axios.post(
        `${BACKEND_URL}/crawl/department`,
        { province: id, contentPage: 'nt', page: pageCrawl },
        options,
      ).then((res) => {
        resolve(res.data);
      }).catch((res) => {
        reject(res);
      });
    }));
    yield* storeNotificationfetchPage(id, infomation);
  } catch (error) {
    console.log('error');
  }
}

export default function* informationSaga() {
  yield all([
    yield takeEvery(INFORMATION.handlers.fetchInfo, fetchInfo),
    yield takeEvery(WSPAGE.handlers.wsfetchPage, wsfetchPage),
    yield takeEvery(NEWSPAGE.handlers.newsPageFetch, newsPageFetch),
    yield takeEvery(NOTIFICATION_PAGE.handlers.notificationfetchPage, notificationfetchPage),
  ]);
}
