import {
  INFORMATION, WSPAGE, NEWSPAGE, NOTIFICATION_PAGE,
} from '../actions/constants';

const initialState = {
  handling: false,
  data: {},
};

export default function catalogs(state = initialState, { type, data, id } = {}) {
  switch (type) {
    case INFORMATION.handlers.fetchInfo:
      return {
        ...state,
        handling: true,
      };

    case INFORMATION.update: {
      const updateData = {
        ...state.data,
        [id]: data,
      };
      return {
        ...state,
        data: updateData,
        handling: false,
      };
    }

    case WSPAGE.handlers.wsfetchPage:
      return {
        ...state,
        handling: true,
      };

    case WSPAGE.update: {
      const updateData = {
        ...state.data,
        [id]: {
          ...state.data[id],
          workingSchedule: {
            ...state.data[id].workingSchedule, ...data.workingSchedule,
          },
        },
      };
      return {
        ...state,
        data: updateData,
        handling: false,
      };
    }

    case NEWSPAGE.handlers.newsPageFetch:
      return {
        ...state,
        handling: true,
      };

    case NEWSPAGE.update: {
      const updateData = {
        ...state.data,
        [id]: {
          ...state.data[id],
          news: {
            ...state.data[id].news, ...data.news,
          },
        },
      };
      return {
        ...state,
        data: updateData,
        handling: false,
      };
    }

    case NOTIFICATION_PAGE.handlers.notificationfetchPage:
      return {
        ...state,
        handling: true,
      };

    case NOTIFICATION_PAGE.update: {
      const updateData = {
        ...state.data,
        [id]: {
          ...state.data[id],
          notifications: {
            ...state.data[id].notifications, ...data.notifications,
          },
        },
      };
      return {
        ...state,
        data: updateData,
        handling: false,
      };
    }

    default:
      return state;
  }
}
