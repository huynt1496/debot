import { Box, Grid } from '@mui/material';
import PropTypes from 'prop-types';
import React from 'react';

export default function Structure({ data }) {
  return (
    <Box>
      <Grid container justifyContent="center">
        <div>
          {data?.type === 'html' ? (
            <Box dangerouslySetInnerHTML={{ __html: data.data }} />
          ) : (
            <img src={data.data} alt="structure" style={{ maxWidth: 1200 }} />
          )}
        </div>
      </Grid>
    </Box>
  );
}

Structure.propTypes = {
  data: PropTypes.shape().isRequired,
};
