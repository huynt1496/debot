import { Box } from '@mui/material';
import Pagination from '@mui/material/Pagination';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';

import { NEWSPAGE } from '../../redux/actions/constants';
import PostComponent from '../PostComponent';

export default function News({ data, unitID }) {
  const [page, pageSet] = useState('1');
  const maxPage = data.totalPage;

  const dispatch = useDispatch();
  const getPage = useCallback(
    (id, pageCrawl) => dispatch({
      type: NEWSPAGE.handlers.newsPageFetch,
      id,
      pageCrawl,
    }),
    [dispatch],
  );

  const handlechangePage = (event, value) => {
    pageSet(`${value}`);
    const dataJson = JSON.stringify(data);
    if (!_.includes(dataJson, `"${value}":[{`)) {
      getPage(unitID, value);
    }
  };

  const renderListNew = (newsPage) => _.map(newsPage, (item, index) => (
    <PostComponent item={item} index={index} key={index} />
  ));

  const newData = data?.[page];

  return (
    <Box>
      {renderListNew(newData)}
      {maxPage > 1
        && (
          <Pagination
            defaultValue="1"
            onChange={handlechangePage}
            count={maxPage}
            showFirstButton
            showLastButton
          />
        )}
    </Box>
  );
}

News.defaultProps = {
  unitID: undefined,
};

News.propTypes = {
  data: PropTypes.shape().isRequired,
  unitID: PropTypes.string,
};
