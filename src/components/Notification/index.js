import { Box } from '@mui/material';
import Pagination from '@mui/material/Pagination';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';

import { NOTIFICATION_PAGE } from '../../redux/actions/constants';
import PostComponent from '../PostComponent';

export default function Notification({
  data, unitID,
}) {
  const [page, pageSet] = useState('1');
  const maxPage = data.totalPage;

  const dispatch = useDispatch();
  const getPage = useCallback(
    (id, pageCrawl) => dispatch({
      type: NOTIFICATION_PAGE.handlers.notificationfetchPage,
      id,
      pageCrawl,
    }),
    [dispatch],
  );

  const handlechangePage = (event, value) => {
    pageSet(`${value}`);
    const dataJson = JSON.stringify(data);
    if (!_.includes(dataJson, `"${value}":[{`)) {
      getPage(unitID, value);
    }
  };

  const renderListNT = (newsPage) => _.map(newsPage, (item, index) => <PostComponent item={item} index={index} key={index} />);
  const newData = data?.[page];

  return (
    <Box>
      {renderListNT(newData)}
      {maxPage > 1
        && <Pagination defaultValue="1" onChange={handlechangePage} count={maxPage} showFirstButton showLastButton />}
    </Box>
  );
}

Notification.propTypes = {
  data: PropTypes.shape({
    totalPage: PropTypes.number,
  }).isRequired,
  unitID: PropTypes.string.isRequired,
};
