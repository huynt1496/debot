import {
  Box, Grid, Typography,
} from '@mui/material';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';

export default function BOD({
  data,
}) {
  if (data.type === 'html') {
    return (
      <Grid container justifyContent="center">
        <Box dangerouslySetInnerHTML={{ __html: data.data }} />
      </Grid>
    );
  }
  return (
    <Box>
      {_.map(data, (item, index) => (
        <Box sx={{ borderBottom: 1, borderColor: 'divider', paddingBottom: 10 }} key={index}>
          <Grid container spacing={2}>
            <Grid item xs={4}>
              {item.avatar
                ? (
                  <img
                    src={item.avatar}
                    alt={`${index}`}
                    loading="lazy"
                    style={{ width: 200 }}
                  />
                ) : null}
            </Grid>
            {item?.title
              ? (
                <Grid item xs={3}>
                  <div style={{ whiteSpace: 'pre-wrap' }}>
                    {(item?.URL_Info)
                      ? (
                        <a href={item?.URL_Info} target="_blank" rel="noreferrer">
                          {item?.title}
                        </a>
                      )
                      : item?.title}
                  </div>
                </Grid>
              )
              : ''}
            <Grid item xs={item?.title ? 5 : 8}>
              <Typography style={{ whiteSpace: 'pre-wrap' }}>{item?.info}</Typography>
            </Grid>
          </Grid>
        </Box>
      ))}
    </Box>
  );
}

BOD.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};
