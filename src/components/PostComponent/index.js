import EventAvailableIcon from '@mui/icons-material/EventAvailable';
import KeyboardDoubleArrowRightRoundedIcon from '@mui/icons-material/KeyboardDoubleArrowRightRounded';
import PersonIcon from '@mui/icons-material/Person';
import VisibilityIcon from '@mui/icons-material/Visibility';
import {
  Box, Grid, Typography,
} from '@mui/material';
import PropTypes from 'prop-types';
import React from 'react';

export default function PostComponent({ item, index }) {
  return (
    <Box
      sx={{ borderBottom: 1, borderColor: 'divider', paddingBottom: 10 }}
      key={index}
    >
      <Grid container spacing={2}>
        <Grid item xs={4}>
          {(item?.avatar)
            ? (
              <a href={item?.URL_title} target="_blank" rel="noreferrer">
                <img
                  src={item?.avatar}
                  alt={`${index}`}
                  loading="lazy"
                  style={{ width: 300 }}
                />
              </a>
            )
            : (
              <Box>
                {(item?.day)
                  ? (
                    <div style={{
                      backgroundImage: 'url(http://cdn.congthongtin.edu.vn/DongNai/img/tbttbg.png)',
                      backgroundSize: '100PX',
                      backgroundRepeat: 'no-repeat',
                      color: '#fff',
                      height: '100px',
                      width: '100px',
                      padding: ' 9px 0',
                      marginRight: '10px',
                      textAlign: 'center',

                    }}
                    >
                      <span>
                        {item?.day}
                      </span>
                      <br />
                      <span>{item?.date}</span>
                    </div>
                  )
                  : null}
              </Box>
            )}
        </Grid>
        <Grid item xs={8}>
          <div style={{ whiteSpace: 'pre-wrap' }}>
            <a
              href={item?.URL_title}
              target="_blank"
              rel="noreferrer"
              style={{ textDecoration: 'none' }}
            >
              <h3 style={{ whiteSpace: 'pre-wrap' }}>{item?.title}</h3>
            </a>

            <Box>
              {(item?.user) ? (
                <Grid item xs={2}>
                  <PersonIcon />
                  {`  ${item?.user}`}
                </Grid>
              ) : <Box />}
              {(item?.date) ? (
                <Grid item xs={4}>
                  <EventAvailableIcon style={{ position: 'absolute' }} />
                  <Typography style={{ marginLeft: 30 }}>{item?.date}</Typography>
                </Grid>
              ) : <Box />}
              {(item?.seen)
                ? (
                  <Grid item xs={4}>
                    <VisibilityIcon />
                    {`  ${item?.seen}`}
                  </Grid>
                ) : <Box />}
            </Box>
            {item?.des}

          </div>
          {item?.more
            ? (
              <a href={item?.URL_title}>
                <strong style={{ color: 'red' }}>
                  {item?.more}
                  <KeyboardDoubleArrowRightRoundedIcon
                    style={{
                      fontSize: '15px',
                    }}
                  />
                </strong>
              </a>
            )
            : null}
        </Grid>
      </Grid>
    </Box>
  );
}
PostComponent.propTypes = {
  item: PropTypes.shape({
    URL_title: PropTypes.string,
    avatar: PropTypes.string,
    title: PropTypes.string,
    user: PropTypes.string,
    seen: PropTypes.string,
    date: PropTypes.string,
    des: PropTypes.string,
    day: PropTypes.string,
    more: PropTypes.string,
  }).isRequired,
  index: PropTypes.number.isRequired,
};
