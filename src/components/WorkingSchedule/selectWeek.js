import {
  Box, Select, MenuItem, FormControl, InputLabel,
} from '@mui/material';
import _ from 'lodash';
import PropTypes, { func } from 'prop-types';
import React from 'react';

export default function SelectWeek({
  year,
  yearSet,
  week,
  weekSet,
  handlechangePage,
}) {
  const date = new Date();
  const currentYear = date.getFullYear();
  const arrYear = _.range(2018, currentYear + 1);
  const arrWeek = _.range(1, 53);

  const changeSelectWeek = (event) => {
    const valueSelect = event.target.value || 0;
    weekSet(valueSelect);
    const valuePage = `${year}${valueSelect} `;
    handlechangePage(event, valuePage);
  };

  const changeSelectYear = (event) => {
    const valueSelect = event.target.value || 0;
    yearSet(valueSelect);
    const valuePage = `${valueSelect}${week} `;
    handlechangePage(event, valuePage);
  };

  return (
    <Box>
      <FormControl sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="year">Chọn năm</InputLabel>
        <Select
          name="year"
          label="Chọn năm"
          labelId="year"
          defaultValue={year}
          onChange={changeSelectYear}
        >
          {arrYear.map((value) => (
            <MenuItem value={value}>{value}</MenuItem>
          ))}
        </Select>
      </FormControl>
      <FormControl sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="week">Chọn tuần</InputLabel>
        <Select
          name="week"
          label="Chọn tuần"
          labelId="week"
          defaultValue={week}
          onChange={changeSelectWeek}
        >
          {arrWeek.map((value) => (
            <MenuItem value={value}>{value}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}

SelectWeek.propTypes = {
  year: PropTypes.shape().isRequired,
  yearSet: PropTypes.shape(func).isRequired,
  week: PropTypes.shape().isRequired,
  weekSet: PropTypes.shape(func).isRequired,
  handlechangePage: PropTypes.shape(func).isRequired,
};
