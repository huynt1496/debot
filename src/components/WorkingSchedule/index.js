import { Box } from '@mui/material';
import Pagination from '@mui/material/Pagination';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';

import { WSPAGE } from '../../redux/actions/constants';
import PostComponent from '../PostComponent';

import SelectWeek from './selectWeek';
import Table from './tablePost';

export default function WorkingSchedule({ data, unitID }) {
  const date = new Date();
  const [week, weekSet] = useState(Number(moment(date).format('W')) + 1);
  const [year, yearSet] = useState(date.getFullYear() || 0);
  const [page, pageSet] = useState(data?.type ? `${year}${week}` : '1');
  const maxPage = data.totalPage;
  const dispatch = useDispatch();
  const getPage = useCallback(
    (id, pageCrawl) => dispatch({
      type: WSPAGE.handlers.wsfetchPage,
      id,
      pageCrawl,
    }),
    [dispatch],
  );

  const handlechangePage = (event, value) => {
    pageSet(`${value}`);
    const dataJson = JSON.stringify(data);
    if (!_.includes(dataJson, `"${value}":[{`)) {
      getPage(unitID, value);
    }
  };

  const renderListWS = (wsPage) => _.map(wsPage, (item, index) => <PostComponent item={item} index={index} key={index} />);

  const renderTableWS = (wsPage) => _.map(wsPage, (item, index) => <Table data={item} key={index} />);

  const newData = data?.[page];

  if (data?.type) {
    return (
      <Box>
        {data?.type === 'html'
          ? <Box dangerouslySetInnerHTML={{ __html: data.data }} />
          : (
            <>
              <SelectWeek
                year={year}
                yearSet={yearSet}
                week={week}
                weekSet={weekSet}
                handlechangePage={handlechangePage}
              />
              {renderTableWS(newData)}
            </>
          )}
      </Box>
    );
  }
  return (
    <Box>
      {renderListWS(newData)}
      {maxPage > 1
        && (
          <Pagination
            defaultValue="1"
            onChange={handlechangePage}
            count={maxPage}
            showFirstButton
            showLastButton
          />
        )}
    </Box>
  );
}

WorkingSchedule.defaultProps = {
  unitID: undefined,
};

WorkingSchedule.propTypes = {
  data: PropTypes.shape().isRequired,
  unitID: PropTypes.string,
};
