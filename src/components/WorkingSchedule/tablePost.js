import {
  Box, TableContainer, TableHead, TableRow, TableCell, TableBody, Table, Paper,
} from '@mui/material';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';

export default function TablePost({
  data,
}) {
  const readerTable = () => (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell variant="head">
              THỨ/NGÀY
            </TableCell>
            <TableCell align="center" variant="head">
              NỘI DUNG CÔNG TÁC
            </TableCell>
            <TableCell variant="head">
              PHÂN CÔNG
            </TableCell>
            <TableCell variant="head">
              PHỐI HỢP
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {_.map(data?.dateOfWeeks, (value) => (
            <TableRow>
              <TableCell>
                {value?.day}
                {`\n${value?.date} `}
              </TableCell>
              <TableCell align="justify">
                {value?.content}
              </TableCell>
              <TableCell>
                {value?.assignment}
              </TableCell>
              <TableCell>
                {value?.combination}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );

  return (
    <Box>
      <Box dangerouslySetInnerHTML={{ __html: data?.content }} />
      {readerTable()}
    </Box>
  );
}

TablePost.propTypes = {
  data: PropTypes.shape().isRequired,
};
