import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import LoadingButton from '@mui/lab/LoadingButton';
import {
  Box,
  Tab,
  Tabs,
  Typography,
  CircularProgress,
  Backdrop,
} from '@mui/material';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toastr } from 'react-redux-toastr';

import {
  AutoComplete,
  BOD,
  Notification,
  News,
  Structure,
  TabPanel,
  WorkingSchedule,
} from '../../components';
import { departmentUrls } from '../../libs/utils';
import { CATALOGS, INFORMATION } from '../../redux/actions/constants';

function tabProps(index) {
  return {
    id: index,
  };
}

export default function Workspace() {
  const { cities, allProvinceInfo, handling } = useSelector(
    ({ catalogs, information }) => ({
      cities: catalogs.cities.data,
      allProvinceInfo: information.data,
      handling: information.handling,
    }),
  );

  const dispatch = useDispatch();
  const handleFetchCities = useCallback(
    () => dispatch({
      type: CATALOGS.handlers.getCities,
    }),
    [dispatch],
  );
  const handleFetchInfo = useCallback(
    (id) => dispatch({
      type: INFORMATION.handlers.fetchInfo,
      id,
    }),
    [dispatch],
  );

  const [selected, selectedSet] = useState();
  const [activeTab, setActiveTab] = useState(0);

  useEffect(() => {
    if (!_.size(cities)) {
      handleFetchCities();
    }
  }, []);

  const handleChange = (event, newValue) => {
    setActiveTab(newValue);
  };

  const handleOpenUrl = () => {
    if (departmentUrls?.[selected]) {
      window.open(departmentUrls[selected].url);
    } else {
      toastr.error('Lỗi', 'Chưa có url của đơn vị này');
    }
  };

  const handleFetchUrl = () => {
    if (departmentUrls?.[selected]?.crawled) {
      handleFetchInfo(selected);
    } else {
      toastr.warning('Thông báo', 'Chưa lấy được dữ liệu của SGD này');
    }
  };

  const citieOptions = _.map(cities, (city, key) => ({ text: city.name, key }));
  const provinceInfo = allProvinceInfo?.[selected];

  return (
    <Box style={{ padding: 20 }}>
      <Backdrop
        sx={{ zIndex: (theme) => theme.zIndex.modal + 1 }}
        open={handling}
      >
        <CircularProgress />
      </Backdrop>

      <Box>
        <Box>
          <Typography style={{ fontWeight: 'bold' }}>
            Chọn Sở giáo dục
          </Typography>
          <AutoComplete
            options={citieOptions}
            value={selected}
            onChange={(value) => selectedSet(value)}
          />
        </Box>
        <LoadingButton
          loading={handling}
          size="small"
          variant="outlined"
          color="primary"
          loadingPosition="start"
          startIcon={<KeyboardArrowRightIcon />}
          style={{ float: 'right', marginTop: 10 }}
          onClick={() => handleFetchUrl()}
        >
          Tra cứu
        </LoadingButton>
        {!!selected
          && (
            <LoadingButton
              size="small"
              variant="outlined"
              color="secondary"
              loadingPosition="start"
              startIcon={<KeyboardArrowRightIcon />}
              style={{ float: 'right', marginTop: 10, marginRight: 20 }}
              onClick={() => handleOpenUrl()}
            >
              Đi đến trang web
            </LoadingButton>
          )}
      </Box>
      {_.size(provinceInfo) ? (
        <Box
          style={{
            marginTop: 50,
            minHeight: 200,
            backgroundColor: '#fff',
            borderRadius: 20,
          }}
          elevation={3}
        >
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={activeTab} onChange={handleChange}>
              <Tab label="Sơ đồ tổ chức" {...tabProps(0)} />
              <Tab label="Ban giám đốc" {...tabProps(1)} />
              <Tab label="Lịch công tác" {...tabProps(2)} />
              <Tab label="Thông báo" {...tabProps(3)} />
              <Tab label="Tin tức - Sự kiện" {...tabProps(4)} />
            </Tabs>
          </Box>
          <TabPanel value={activeTab} index={0}>
            <Structure data={provinceInfo?.structure} />
          </TabPanel>
          <TabPanel value={activeTab} index={1}>
            <BOD data={provinceInfo?.bod} />
          </TabPanel>
          <TabPanel value={activeTab} index={2}>
            <WorkingSchedule
              unitID={selected}
              data={provinceInfo?.workingSchedule}
            />
          </TabPanel>
          <TabPanel value={activeTab} index={3}>
            <Notification
              unitID={selected}
              data={provinceInfo?.notifications}
            />
          </TabPanel>
          <TabPanel value={activeTab} index={4}>
            <News unitID={selected} data={provinceInfo?.news} />
          </TabPanel>
        </Box>
      ) : null}
    </Box>
  );
}

Workspace.defaultProps = {
  cities: undefined,
  provinceInfo: undefined,
};

Workspace.propTypes = {
  cities: PropTypes.shape({}),
  provinceInfo: PropTypes.shape({}),
};
